# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: auth.proto
# Protobuf Python Version: 4.25.0
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\nauth.proto\"/\n\x04User\x12\n\n\x02id\x18\x01 \x01(\x03\x12\r\n\x05\x65mail\x18\x02 \x01(\t\x12\x0c\n\x04role\x18\x03 \x01(\t\"6\n\x13RegistrationRequest\x12\r\n\x05\x65mail\x18\x01 \x01(\t\x12\x10\n\x08password\x18\x02 \x01(\t\"\'\n\x14RegistrationResponse\x12\x0f\n\x07user_id\x18\x01 \x01(\x03\"/\n\x0cLoginRequest\x12\r\n\x05\x65mail\x18\x01 \x01(\t\x12\x10\n\x08password\x18\x02 \x01(\t\"Q\n\rLoginResponse\x12\x14\n\x0c\x61\x63\x63\x65ss_token\x18\x01 \x01(\t\x12\x15\n\rrefresh_token\x18\x02 \x01(\t\x12\x13\n\x04user\x18\x03 \x01(\x0b\x32\x05.User\"-\n\x14RefreshTokensRequest\x12\x15\n\rrefresh_token\x18\x01 \x01(\t\"D\n\x15RefreshTokensResponse\x12\x14\n\x0c\x61\x63\x63\x65ss_token\x18\x01 \x01(\t\x12\x15\n\rrefresh_token\x18\x02 \x01(\t2\xba\x01\n\rAuthorization\x12=\n\x0cRegistration\x12\x14.RegistrationRequest\x1a\x15.RegistrationResponse\"\x00\x12(\n\x05Login\x12\r.LoginRequest\x1a\x0e.LoginResponse\"\x00\x12@\n\rRefreshTokens\x12\x15.RefreshTokensRequest\x1a\x16.RefreshTokensResponse\"\x00\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'auth_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  _globals['_USER']._serialized_start=14
  _globals['_USER']._serialized_end=61
  _globals['_REGISTRATIONREQUEST']._serialized_start=63
  _globals['_REGISTRATIONREQUEST']._serialized_end=117
  _globals['_REGISTRATIONRESPONSE']._serialized_start=119
  _globals['_REGISTRATIONRESPONSE']._serialized_end=158
  _globals['_LOGINREQUEST']._serialized_start=160
  _globals['_LOGINREQUEST']._serialized_end=207
  _globals['_LOGINRESPONSE']._serialized_start=209
  _globals['_LOGINRESPONSE']._serialized_end=290
  _globals['_REFRESHTOKENSREQUEST']._serialized_start=292
  _globals['_REFRESHTOKENSREQUEST']._serialized_end=337
  _globals['_REFRESHTOKENSRESPONSE']._serialized_start=339
  _globals['_REFRESHTOKENSRESPONSE']._serialized_end=407
  _globals['_AUTHORIZATION']._serialized_start=410
  _globals['_AUTHORIZATION']._serialized_end=596
# @@protoc_insertion_point(module_scope)
