from http_server.http_server import HTTP_Server


class App:
    def __init__(self, http_server: HTTP_Server) -> None:
        self.http_server = http_server
