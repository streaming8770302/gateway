from fastapi import FastAPI

import config
from app.app import App
from grpc_client.auth.client import AuthClient
from grpc_client.grpc_client import GRPCClient
from http_server.http_server import HTTP_Server

if __name__ == "__main__":
    auth_client = AuthClient(config.AUTH_SERVICE_PORT)
    grpc_client = GRPCClient(auth_client)

    http_server = HTTP_Server(
        router=FastAPI(),
        auth_client=auth_client)

    app = App(http_server)
    app.http_server.run()
