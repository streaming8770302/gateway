activate:
	source ./venv/bin/activate

lint:
	isort .

requirements:
	pip freeze > requirements.txt

install_deps:
	pip install -r requirements.txt

proto_generate:
	python -m grpc_tools.protoc -I ./proto --python_out=./pb/auth --pyi_out=./pb/auth --grpc_python_out=./pb/auth ./proto/auth.proto