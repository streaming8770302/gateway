import grpc

import pb.auth.auth_pb2 as auth_pb2
from pb.auth.auth_pb2_grpc import AuthorizationStub


class AuthClient:
    def __init__(self, port: str) -> None:
        address = f"localhost:{port}"
        channel = grpc.insecure_channel(address)
        stub = AuthorizationStub(channel)

        self.stub = stub

    def registration(self, email: str, password: str) -> auth_pb2.RegistrationResponse:
        response = self.stub.Registration(auth_pb2.RegistrationRequest(email=email, password=password))
        print(response)
        return response
    
    def login(self, email: str, password: str) -> auth_pb2.LoginResponse:
        response = self.stub.Login(auth_pb2.LoginRequest(email=email, password=password))
        print(response)
        return response
    
    def refresh_tokens(self, refresh_token: str) -> auth_pb2.RefreshTokensResponse:
        response = self.stub.RefreshTokens(auth_pb2.RefreshTokensRequest(refresh_token=refresh_token))
        print(response)
        return response