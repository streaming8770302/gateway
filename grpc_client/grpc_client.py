from grpc_client.auth.client import AuthClient


class GRPCClient:
    def __init__(self, auth_client: AuthClient) -> None:
        self.auth_client = auth_client

    