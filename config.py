import os

from dotenv import load_dotenv

load_dotenv()

origins = [
    os.environ.get("ORIGINS")
]

GATEWAY_PORT = int(os.environ.get("GATEWAY_PORT"))
AUTH_SERVICE_PORT = os.environ.get("AUTH_SERVICE_PORT")
