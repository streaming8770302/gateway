import uvicorn
from fastapi import FastAPI, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import BaseModel

from config import *
from grpc_client.auth.client import AuthClient
from logger_opensearch import logger

origins = [
    "http://localhost:5173/",
    "http://localhost:5174/",
]

class User(BaseModel):
    email: str
    password: str

class HTTP_Server:
    def __init__(self, router: FastAPI, auth_client: AuthClient) -> None:
        self.router = router
        self.auth_client = auth_client
        Instrumentator().instrument(self.router).expose(self.router)


        self.configure_routes()

    def configure_routes(self):
        self.router.add_middleware(
            CORSMiddleware,
            allow_methods=["GET", "POST", "PUT", "DELETE", "PATCH"],
            allow_origins=["*"],
            allow_headers=["*"],
        )

        @self.router.post("/registration")
        async def registration(user: User):
            response = self.auth_client.registration(user.email, user.password)
            if response != -1:
                logger.info("User {user} was registrated")
                return JSONResponse(content={"message": "Successful registration"}, status_code=status.HTTP_200_OK)

        @self.router.post("/login")
        async def login(user: User):
            response_rpc = self.auth_client.login(user.email, user.password)
        
            if response_rpc.access_token == "":
                logger.error("User {user} error logged in")
                return JSONResponse(content={"message": "Error logging in."}, status_code=status.HTTP_204_NO_CONTENT)
            
            response = JSONResponse(
                content={
                    "access_token": response_rpc.access_token,
                    "user": {
                        "id": response_rpc.user.id,
                        "email": response_rpc.user.email,
                        "role": response_rpc.user.role,
                    },
                },
                status_code=status.HTTP_200_OK)
            
            response.set_cookie(
                key="refresh_token",
                value=response_rpc.refresh_token,
                httponly=True)
            logger.info("User {user} was logged in with tokens: access key: {access_key}, refresh key: {refresh_key}")
            return response

        @self.router.get("/logout")
        async def logout(response: Response):
            response.delete_cookie(key="refresh_token")
            logger.info("User successfuly logged out")
            return JSONResponse(content={"message": "Successful logged out."}, status_code=status.HTTP_200_OK)

        @self.router.get("/refresh_token")
        async def refresh_token(request: Request):
            response_rpc = self.auth_client.refresh_tokens(request.cookies.get("refresh_token"))
            response = JSONResponse(
                content={
                    "access_token": response_rpc.access_token},
                status_code=status.HTTP_200_OK)

            response.set_cookie(
                key="refresh_token",
                value=response_rpc.refresh_token,
                httponly=True)
            logger.info("User refresh token")
            return response

    def run(self):
        uvicorn.run(self.router, host="0.0.0.0", port=int(GATEWAY_PORT))
